# XML2RDF #

Scala application developed in eight hours and a half as part of a technical exercise during a selection proccess. The description of the exercise  goes as follows:

"The exercise is simple:

* Use the example XML data available at https://db.tt/8GhgujNV
* Use whatever technology you think is better for transforming that data into RDF.
* Try to get as much useful data out of it (addresses, images, locations, etc).
* Do not spend too much time thinking about vocabulary, use mainly schema.org classes and properties.
* Optionally interlinking it with any external datasets (Freebase, DBpedia, Wikidata, Geonames, etc) you may have available.

Some hints about the data:

* It is about accommodations (hotels and so on) in Salzburg.
* Each accommodation is under the path /FeratelDsiRS/Result/ServiceProviders/ServiceProvider"
###Approach description###

The Idea is to split the XML file in to chunks of 60MB aprox, containing one service provider element each of them. Those chunks would be loaded as POJO's and persisted to RDF data model one at a time, giving priority to memory performance aginst time.

### Technology used for the task ###

* Scala 2.10.5 + sbt 0.13
* scala-scales (for big XML handling)
* STax (same as above)
* Jena (RDF modeling and triplestore)
* Grizzled + logback (for logging)
* Specs2 (for testing - FAIL)
* ScalaTest (for testing)

#### Technology discussion ####

**Scala**: it  was selected for this task for one main reason: I am more used to program with it so, having a limited amount of time to solve the problem, it was suppossed to make it easier to fulfil the task in time.

**scalesXML**: to work with such a big XML file it was clear a subdivision of the file was needed. To do so two technologies were outlinined, scalesXML and STax. The first was for scala so it should probably be easier to work with and no conversions would be needed, and the performance results comparing to other libraries were good[1].

**Jena**: the same as Scala, Jena was mainly choosen because I had experience with it so it wouldn't take much time to export a model into it and publish it as RDF.

**ScalaTest**: for testing, Specs2 was the first option due to its natural way of describing the specific tests as seen below, but dependency version problems between ScalesXML andSpecs2 appear so it had to be replaced by ScalaTest.

```
#!scala

 def is = s2"""
 This is a specification to check the 'Hello world' string
 The 'Hello world' string should
   contain 11 characters                             $e1
   start with 'Hello'                                $e2
   end with 'world'                                  $e3
                                                     """

  def e1 = "Hello world" must haveSize(11)
  def e2 = "Hello world" must startWith("Hello")
  def e3 = "Hello world" must endWith("world")

```

### What's working? ###
Right now the XML2RDF code is able to read the XML, turn the service providers as POJO's and loading them to the model to be published as RDF(any jena suported format) in to a file or to the console. As said in the description, memory performance was given priority against speed, turning out to be slower than spected: 21 minutes to process the complete file -some time here is needed to improve speed performance -.

Though the XML modeling hasn't been completed so far, for a given service provider just the Id and details are colected as POJO's  and not all their properties were modeled as RDF neither.
Those easy but time consuming tasks (defining POJO for each sub element of provider or finding the right RDF properties for each atribute) were left behind in order to solve problems faced during the process so the full cycle of publishing from XML to RDF was completed.

### Problems found ###

I was decided to mix the native scalaXML functionalities to handle the chunks of XML, due to its natural way of working.


```
#!scala

val xmlNode= 
<node>
  <elem>
    <subelem>content text</subelem>
  </elem>
</node>

val title = xmlNode \ "elem" \ "subelem"
```


 Spliting the file in smaller elements task will be aproached with the scalesXML memory saving capabilities to split the file on those elements, but it resulted to be not as easy as expected finding the following difficulties:

* the scalesXML Elem were **not so direct** to be parsed as scalXML nodes, fixed by giving a not so hard but not clean at all coding solution to the problem.
* the scalesXML library choosen for the file spliting process generated **dependency version conflicts** with the Specs2 testing library dependencies. At the time of finding the problem some tests were alredy done, so some time was spent trying to fix it with no positive results. Finally the testing library was repalced by ScalaTest and test redone.

### What now? where to go ###
The nexts steps would be to:

* Reduce the** 21 minutes** time processing by removing scalaXML from the code and creating the POJOs based on scalesXML Elem's. If the result is not fast enough, study the posibility of using some concurrency toll like Akka to work with more than one provider at a time.
* **Conclude the POJO and RDF models**. As told before these tasks were left behind to be able to finish the whole cycle, so a further work on defining the provider subelements as Scala classes and how they should be modeled in to RDF is needed.
* **Model interlinking** with external sources should be perform to enrich the generated RDF model.  To acomplish this several option could be used such as:

- tabels[2], to identify posible resources similar to the ones existing in the model by textual similarity (it uses Lucene functions for text processing and finding matching termes) with DBpedia resources.
- OpenRefine Reconciliation-service-API[3], to perform entity reconciliation aginst Freebase
- RDF Refine[4], plugin done by DERI extending OpenRefine to be able to reconciliate against any SPARQL endpoint.
### How do I get set up? ###

* Install Git[5] and clone this repository:

```
#!git

git clone git@bitbucket.org:alfonso_noriega/xml2rdf.git
```

* To run the code it is needed Scala 2.10.5[6] and sbt[7]
* To see it work go to the project main folder and run the next command from the shell  :

```
#!sbt

sbt test-only org.example.AccomodationITTest
```

This **will take 21 minutes** while running through the original XML file and generating the RDF model which will be print in the console.

* How to run tests

### Who do I talk to? ###

* Alfonso Noriega Meneses - alfonso.noriega.meneses@gmail.com

[1]http://scala-scales.googlecode.com/svn/sites/scales/scales-xml_2.10/0.5.0/MemoryOptimisation.html

[2]http://idi.fundacionctic.org/tabels/

[3]https://github.com/OpenRefine/OpenRefine/wiki/Reconciliation-Service-API

[4]http://refine.deri.ie/

[5]http://git-scm.com/

[6]http://www.scala-lang.org/download/2.10.5.html

[7]http://www.scala-sbt.org/