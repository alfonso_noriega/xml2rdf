package org.example.publishers

/**
 * Created by fonso on 15/03/15.
 */

import java.io.{FileOutputStream, OutputStream}
import java.net.URI

import com.hp.hpl.jena.rdf.model.{Model, ModelFactory}

class JenaDataOutput(prefixes : Map[String,URI] = Map()) {

  val model : Model = ModelFactory.createDefaultModel()
  val possibleFormats = Set("RDF/XML", "RDF/XML-ABBREV", "N-TRIPLE", "TURTLE", "TTL", "N3")

  prefixes.foreach { case (prefix,ns) => model.setNsPrefix(prefix, ns.toString) }

  def generateOutput(outFile:Option[String], outFormat:String = "RDF/XML")  {

    val outputFormatValidated = outFormat match {
      case validFormat if possibleFormats.contains(validFormat) => validFormat
      case _ => throw new IllegalArgumentException(s"Wrong output format: $outFormat")
    }

    val os : OutputStream = if (outFile.nonEmpty) new FileOutputStream(outFile.get) else System.out
    model.write(os, outputFormatValidated)

  }

}