package org.example.readers

import java.io.FileReader

import com.hp.hpl.jena.rdf.model.{ModelFactory, Model}
import grizzled.slf4j.Logging
import org.example.pojos
import org.example.pojos.{Details, ServiceProvider}
import scales.xml.ScalesXml._
import scales.xml._

import scala.Predef.any2ArrowAssoc
import scala.xml.{NodeSeq, XML}

/**
 * Created by fonso on 14/03/15.
 */
class BigXMLReader extends Logging{

  def readXML(filePath:String, model :Model): Seq[ServiceProvider] ={

    val pull = pullXml(new FileReader(filePath))
    var xmlNode =""

    while (pull.hasNext){
      pull.next match{
        case Left(i:XmlItem) => xmlNode +=i.value

        case Left(e : Elem)  => e.name.local match {
              case "FeratelDsiRS"|"Result"|"ServiceProviders" => logger.trace("#------> Ignoring header: " + e.name.qName)
              case "ServiceProvider" =>{
                xmlNode =  "<" + e.name.qName

                e.attributes.map(attribute => {
                  val prefixSeparator = if (attribute.hasPrefix) ":" else ""
                  xmlNode += " " + attribute.prefix.getOrElse("") + prefixSeparator + attribute.local + "=\"" + attribute.value + "\""
                })
                xmlNode += ">"
              }
              case _ => {
                logger.trace("#------> provider SubElement: " + e.name.qName)
                xmlNode += "<" + e.name.qName
                e.attributes.map(attribute => {
                  val prefixSeparator = if (attribute.hasPrefix) ":" else ""
                  xmlNode += " " + attribute.prefix.getOrElse("") + prefixSeparator + attribute.local + "=\"" + attribute.value + "\""
                })
                xmlNode += ">"

              }
             }

        case Right(endElem)  => endElem.name.local match{
            case "FeratelDsiRS"|"Result"|"ServiceProviders" => logger.trace("#------> Ignoring closing header: " + endElem.name.qName)
            case "ServiceProvider" => {
              xmlNode += "</"+endElem.name.qName +">"
              ServiceProvider.fromXML(XML.loadString(xmlNode)).toRDF(model)
              logger.trace("#------> closing  provider" )

            }
            case _ =>  xmlNode += "</"+endElem.name.qName +">"
        }
      }
    }

    pull.close()

    return  Seq()
  }

}
