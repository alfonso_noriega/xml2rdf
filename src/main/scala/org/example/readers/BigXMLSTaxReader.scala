package org.example.readers

import java.io.FileReader
import javax.xml.stream.{XMLStreamConstants, XMLInputFactory}

import grizzled.slf4j.Logging
import org.example.pojos.ServiceProvider
import scales.xml._

import scala.xml.XML

/**
 * Created by fonso on 15/03/15.
 */
class BigXMLSTaxReader extends Logging{

  def readXML(filePath:String): Seq[ServiceProvider] ={

    val xmlFactory = XMLInputFactory.newInstance()
    val xmlReader = xmlFactory.createXMLStreamReader(new FileReader(filePath))

    try {
      xmlReader.nextTag()
      xmlReader.nextTag()
      xmlReader.nextTag()
      xmlReader.require(XMLStreamConstants.START_ELEMENT,null,"ServiceProviders")

      xmlReader.nextTag()

      while (xmlReader.getEventType == XMLStreamConstants.START_ELEMENT) {

        logger.error("#------>"+ xmlReader.getPITarget)
        ServiceProvider.fromXML(XML.load(xmlReader.toString))

        if(xmlReader.getEventType == XMLStreamConstants.CHARACTERS)
          xmlReader.next()
      }
    }finally xmlReader.close()

    return  Seq()


  }
}
