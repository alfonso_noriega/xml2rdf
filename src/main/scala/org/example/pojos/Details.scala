package org.example.pojos

/**
 * Created by fonso on 14/03/15.
 */

import com.hp.hpl.jena.rdf.model.{Resource, Model}
import grizzled.slf4j.Logging

import scala.xml.NodeSeq

class Details(
               val name:String,
               val town:String,
               val district:String,
               val code:String,
               val accomodationType:String,
               val priority:Int,
               val rooms:Int,
               val beds: Int,
               val position:Position,
               val categories: Seq[String],
               val classifications: Seq[String],
               val marketingGroups: Seq[String],
               val active: Boolean,
               val creditCards: Seq[String],
               val planNumber: String,
               val dbCode: String,
               val bookable: Boolean,
               val bankAccounts: Seq[String],
               val currencyCode: String) extends Logging{

  def toRDF(model:Model, parent:Resource)  ={


    // schema: name
    model.add(model.createStatement(parent,
      model.createProperty("http://schema.org/","name"),
      model.createLiteral(this.name))
    )

    // schema: location
    val placeResource = model.createResource("http://org.example/accomodations/place#"+
      this.code)
    model.add(model.createStatement(parent,
      model.createProperty("http://schema.org/","location"),
      placeResource)
    )


    // rdf:Type statement
    model.add(model.createStatement(placeResource,
      model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#","type"),
      model.createResource("http://schema.org/Place"))
    )



    this.position.toRDF(model,placeResource)

    logger.trace("#------>Upgrading model with details of: " + this.name)


  }
}
object Details{
  def fromXML(detailsNode: NodeSeq): Details =
    new Details(
      name = (detailsNode \ "Name").text ,
      town = (detailsNode \ "Town").text ,
      district = (detailsNode \ "District").text ,
      code  = (detailsNode \ "Code").text ,
      accomodationType= (detailsNode \ "Type").text ,
      priority = (detailsNode \ "Priority").text toInt,
      rooms = (detailsNode \ "Rooms").text toInt ,
      beds = (detailsNode \ "Beds").text toInt,
      position = Position.fromXML((detailsNode \ "Position")) ,
      categories = (detailsNode \ "Categories" \ "Item").toList map{ category => (category \ "@Id").text} ,
      classifications = (detailsNode \ "Classifications" \ "Item").toList map{ classification => (classification \ "@Id").text} ,
      marketingGroups = (detailsNode \ "MarketingGroups" \ "Item").toList map{ mGroup => (mGroup \ "@Id").text} ,
      active = (detailsNode \ "Active").text toBoolean ,
      creditCards = (detailsNode \ "CreditCards" \ "Item").toList map{ creditCard => (creditCard \ "@Id").text} ,
      planNumber = (detailsNode \ "PlanNumber").text ,
      dbCode = (detailsNode \ "DBCode").text ,
      bookable = (detailsNode \ "Bookable").text toBoolean ,
      bankAccounts = (detailsNode \ "BankAccounts" \ "Item").toList map{ bankAccount => (bankAccount \ "@Id").text} ,
      currencyCode= (detailsNode \ "CurrencyCode").text
    )
}
