package org.example.pojos

import com.hp.hpl.jena.rdf.model.{Resource, Model}
import grizzled.slf4j.Logging

import scala.xml.NodeSeq

/**
 * Created by fonso on 14/03/15.
 */
class ServiceProvider( val id:String,
                       val changeDate:String,
                       val details: Details
                       /*documents: Seq[Document],
                       descriptions: Seq[Description],
                       links: Seq[Link],
                       facilities: Seq[Facility],
                       addresses: Seq[Address],
                       services: Seq[Service]*/
                       ) extends Logging{

  def toRDF(model: Model) = {

      val serviceProviderResource = model.createResource("http://org.example/accomodations/serviceProvider#"+
      this.id)

      // rdf:Type statement
      model.add(model.createStatement(serviceProviderResource,
      model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#","type"),
      model.createResource("http://schema.org/LocalBusiness"))
      )

      this.details.toRDF(model,serviceProviderResource)

  }
}

object ServiceProvider{
def fromXML(nodeProvider: NodeSeq)=
new ServiceProvider(
id = (nodeProvider \ "@Id").toString,
changeDate =(nodeProvider \ "@ChangeDate").toString,
details = Details.fromXML(nodeProvider \ "Details")
)
}