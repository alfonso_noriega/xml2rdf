package org.example.pojos

import com.hp.hpl.jena.rdf.model.{Model, Resource}

import scala.xml.NodeSeq

/**
 * Created by fonso on 14/03/15.
 */
class Position(val latitude:String, val longitude:String) {

  def toRDF(model:Model, parent:Resource)  = {

    val geoCoordinatesResource = model.createResource("http://org.example/accomodations/geocoordinates#"+
      this.latitude.replace(".","_") + this.longitude.replace(".","_"))


    // schema:geo statement
    model.add(model.createStatement(parent,
      model.createProperty("http://schema.org/","geo"),
     geoCoordinatesResource)
    )

    // rdf:Type statement
    model.add(model.createStatement(geoCoordinatesResource,
      model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#","type"),
      model.createResource("http://schema.org/GeoCoordinates"))
    )
    model.add(model.createStatement(geoCoordinatesResource,
      model.createProperty("http://schema.org/","latitude"),
      model.createLiteral(this.latitude))
    )
    model.add(model.createStatement(geoCoordinatesResource,
      model.createProperty("http://schema.org/","longitude"),
      model.createLiteral(this.longitude))
    )

  }
}

object Position{
  def fromXML(nodePosition:NodeSeq):Position =
    new Position(
      latitude = (nodePosition \ "@Latitude").text,
      longitude = (nodePosition \ "@Longitude").text)


}
