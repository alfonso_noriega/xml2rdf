package org.example.pojos

import org.scalatest.FlatSpec

/**
 * Created by fonso on 14/03/15.
 */
class DetailsTest extends FlatSpec {

  //Simple Details POJO test
  val detailsXML =
    <Details>
      <Name>Haslach, Appartements</Name>
      <Town>48da9803-0752-4b91-9a57-ae5e736ba7a1</Town>
      <District>5194f3b1-d062-48bc-b935-aa041423224a</District>
      <Code>B8363</Code>
      <Type>Accommodation</Type>
      <Priority>99</Priority>
      <Rooms>0</Rooms>
      <Beds>0</Beds>
      <Position Latitude="47.282184081114622" Longitude="12.35936164855957" />
      <Categories>
        <Item Id="EC3C8E0C-961C-4CFD-900E-1F2875ACCE63" />
        <Item Id="E4F3C2AC-FACD-4A14-A266-3502370D6599" />
      </Categories>
      <Classifications>
        <Item Id="3BAC3E97-3432-4B89-88BD-685D5827ECEC" />
      </Classifications>
      <MarketingGroups>
        <Item Id="F753696B-6376-46AF-8618-3D8568F74766" />
      </MarketingGroups>
      <Active>true</Active>
      <CreditCards />
      <GridSquare />
      <PlanNumber />
      <DBCode>SBG</DBCode>
      <Bookable>true</Bookable>
      <BankAccounts />
      <CurrencyCode>EUR</CurrencyCode>
    </Details>

  val detailsTest = Details.fromXML(detailsXML)

  "The fromXML method" should "generate a Details POJO instance with" +
    " name 'Haslach, Appartements'"  in {

      assert(detailsTest.name == "Haslach, Appartements")// must be equalTo("Haslach, Appartements")
    }

  "The fromXML method" should "generate a Details POJO instance with two categories " +
      "item 'EC3C8E0C-961C-4CFD-900E-1F2875ACCE63' and 'E4F3C2AC-FACD-4A14-A266-3502370D6599'"  in {
      assert(detailsTest.categories.size == 2)// must be equalTo(2)
      assert(detailsTest.categories(0) == "EC3C8E0C-961C-4CFD-900E-1F2875ACCE63" )// must be equalTo("EC3C8E0C-961C-4CFD-900E-1F2875ACCE63")
      assert(detailsTest.categories(1) == "E4F3C2AC-FACD-4A14-A266-3502370D6599" )// must be equalTo("E4F3C2AC-FACD-4A14-A266-3502370D6599")
    }

  "The fromXML method" should "generate a Details POJO instance with one clasification " +
      "item '3BAC3E97-3432-4B89-88BD-685D5827ECEC'"  in {
      assert(detailsTest.classifications.size == 1)//must be equalTo(1)
      assert(detailsTest.classifications(0) == "3BAC3E97-3432-4B89-88BD-685D5827ECEC")//must be equalTo("3BAC3E97-3432-4B89-88BD-685D5827ECEC")
    }

  "The fromXML method" should  "generate a Position POJO within details instance with " +
      "latitude '47.282184081114622'"  in {
      assert(detailsTest.position.latitude == "47.282184081114622")// must be equalTo("47.282184081114622")
    }

  "The fromXML method" should "generate a Position POJO instance with " +
      "longitude '12.35936164855957'"  in {
      assert(detailsTest.position.longitude == "12.35936164855957")// must be equalTo("12.35936164855957")
    }

  "The fromXML method" should "generate a Details POJO instance active"  in {
      assert(detailsTest.active == true)// must be equalTo(true)
    }

  "The fromXML method" should "generate a Details POJO instance with no bank account " +
      "items"  in {
      assert(detailsTest.classifications.size == 0)// must be equalTo(0)
    }


}
