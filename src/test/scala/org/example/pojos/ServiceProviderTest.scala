package org.example.pojos

import org.scalatest.FlatSpec

import scala.xml.XML
/**
 * Created by fonso on 14/03/15.
 */
class ServiceProviderTest extends FlatSpec{

  val providerDetailsXML = XML.loadFile(this.getClass.getResource("/singleServiceProvider.xml").getFile.replace("%20"," "))
  //val providerDetailsXML = loadXml(new FileReader(this.getClass.getResource("/singleServiceProvider.xml").getFile.replace("%20"," ")))

  "The fromXML method" should "generate a ServiceProvider POJO instance" +
    " with a Details POJO named 'Haslach, Appartements'"  in {
    val providerTest = ServiceProvider.fromXML(providerDetailsXML \\ "ServiceProvider")
    assert(providerTest.details.name == "Haslach, Appartements")// must be equalTo("Haslach, Appartements")
    }

}
