package org.example.pojos

import org.scalatest.FlatSpec

/**
 * Created by fonso on 14/03/15.
 */
class PositionTest extends FlatSpec{

  //Simple Position POJO test
  val positionXML = <Position Latitude="0.0" Longitude="0.0" />


  "The fromXML method" should "generate a Position POJO instance with latitude '0.0'"  in {
    val positionTest = Position.fromXML(positionXML)

    assert(positionTest.latitude == "0.0") //must be equalTo("0.0")
    }

  "The fromXML method" should "generate a Position POJO instance with longitude '0.0'"  in {
    val positionTest = Position.fromXML(positionXML)

    assert(positionTest.longitude == "0.0") //must be equalTo("0.0")
  }


}
