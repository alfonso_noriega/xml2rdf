package org.example.readers

import com.hp.hpl.jena.rdf.model.ModelFactory
import org.scalatest.FlatSpec

/**
 * Created by fonso on 15/03/15.
 */
class BigXMLReaderTest extends FlatSpec {

  val accomodationsFilePath = this.getClass.getResource("/accomodations.xml").getFile.replace("%20", " ")

  val readed = new BigXMLReader().readXML(accomodationsFilePath, ModelFactory.createDefaultModel())

  "The readXML method" should "return a list of 1 elements " +
    "when reading accomodations file" in {

    assert(readed.size == 1) // must be equalTo(10)
  }

  "The readXML method" should "return a list of n elements " +
    "when reading accomodations file" in {

    assert(readed.seq(0).details.active == true) // must be equalTo(10)
  }

}