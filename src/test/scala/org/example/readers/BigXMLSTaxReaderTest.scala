package org.example.readers

import org.scalatest.FlatSpec

/**
 * Created by fonso on 15/03/15.
 */
class BigXMLSTaxReaderTest extends FlatSpec {

  val accomodationsFilePath = this.getClass.getResource("/singleServiceProvider.xml").getFile.replace("%20", " ")

  val readed = new BigXMLSTaxReader().readXML(accomodationsFilePath)

  "The readXML method" should "return a list of 1 elements " +
    "when reading accomodations file" in {

    assert(readed.size == 1) // must be equalTo(10)
  }

  "The readXML method" should "return a list of n elements " +
    "when reading accomodations file" in {

    assert(readed.seq(0).details.active == true) // must be equalTo(10)
  }


}
