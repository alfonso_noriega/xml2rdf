package org.example.publishers

import org.scalatest.FlatSpec

import scala.xml.XML
/**
 * Created by fonso on 14/03/15.
 */
class JenaDataOutputTest extends FlatSpec {

  val jenaModel = new JenaDataOutput
  val serviceProviderResource = jenaModel.model.createResource("http://org.example/testJenaResource")

  // rdf:Type statement
  jenaModel.model.add(jenaModel.model.createStatement(serviceProviderResource,
    jenaModel.model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#","type"),
    jenaModel.model.createResource("http://org.example/testJenaObject"))
  )

  "The generateOutput method" should "without an output file write the model" +
    " in to the console"  in {
    val modelTest = jenaModel.generateOutput(None)
  }

  "The generateOutput method" should "with an output file write the model" +
    " in to the output file"  in {
    val fileOutput = this.getClass.getResource("/out.rdf").getFile.replace("%20", " ")
    val modelTest = jenaModel.generateOutput(Option(fileOutput))
  }

}
