package org.example

import java.net.URI
import java.util

import org.example.publishers.JenaDataOutput
import org.example.readers.BigXMLReader
import org.scalatest.FlatSpec

import scala.collection.immutable.HashMap

/**
 * Created by alfonso on 17/03/15.
 */
class AccomodationITTest extends FlatSpec {

  val jenaModel = new JenaDataOutput(Map()+("schema" -> new URI("http://schema.org/"),"example" -> new URI("http://example.org/")))

  val accomodationsFilePath = this.getClass.getResource("/accomodations.xml").getFile.replace("%20", " ")

  val readedAccomodations = new BigXMLReader().readXML(accomodationsFilePath, jenaModel.model)

  "The complete XML process" should "have generated a model of size 40594" in {
    jenaModel.generateOutput(None)
    assert (jenaModel.model.size == 40594)
  }


}
