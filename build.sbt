import sbt.ExclusionRule

name := "XML2RDF"

version := "1.0"

scalaVersion := "2.10.5"

resolvers += "Scales Repo" at "http://scala-scales.googlecode.com/svn/repo"

// logging: grizzled + logback
libraryDependencies += "org.clapper" % "grizzled-slf4j_2.10" % "1.0.1"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.7"


//XML scala library design for high volume files
libraryDependencies += "org.scalesxml"%%"scales-xml"%"0.5.0"

libraryDependencies += "org.scalesxml"%%"scales-jaxen"%"0.5.0" intransitive()

libraryDependencies += "jaxen"%"jaxen"%"1.1.3" intransitive()

libraryDependencies += "org.scalesxml"%%"scales-aalto"%"0.5.0"

//Jena
libraryDependencies += "com.hp.hpl.jena" % "jena" % "2.6.4"

libraryDependencies += "com.hp.hpl.jena" % "arq" % "2.8.8"

libraryDependencies += "com.hp.hpl.jena" % "tdb" % "0.8.10"

// Scala test tools -> Apache License Version 2.0
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.1" % "test"
